
import './App.css';

import cartoon from './cartoon.png';

function App() {
  return (
    <div>
      <h1>This is the course booking of batch 164</h1>
      <h5>This is our project in React</h5>
      <h6>Come Visit our website</h6> 
      {/*This image came from the /src folder*/}
      <img src={cartoon} alt="image not found" />

      <img src="/image/cartoon.png" />
      <h3>This is My Favorite Cartoon Character</h3>  
    </div>
    );
}

export default App;
